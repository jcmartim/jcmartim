const headerContent = `<h1>Design e desenvolvimento de aplicativos Web e Mobile</h1>
<div class="back-menu">
    <div class="box-menu">
        <a href="index.html"><div class="logo"><h2>jcmartim</h2></div></a>
        <div class="boxmenu">
            <div class="toggle"></div>
            <a href="#" id="toggle"><span></span></a>
            <nav class="menu">
                <ul>
                    <li><a href="https://jcmartim.com.br/servicos">Serviços</a></li>
                    <li><a href="https://jcmartim.com.br/trabalhos">Trabalhos</a></li>
                    <li><a href="https://jcmartim.com.br/processo">Processo</a></li>
                    <li><a href="https://jcmartim.com.br/carreiras">Carreiras</a></li>
                    <li><a href="https://jcmartim.com.br/contato">Contato</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<aside id="m-mobile" class="menu-mobile">
    <ul>
        <li>
            <a class="menu-mobile-icons" href="https://jcmartim.com.br">
                <svg class="bi bi-white mr-2" width="32" height="32" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M9.646 3.146a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4.5a.5.5 0 01-.5-.5v-4H9v4a.5.5 0 01-.5.5H4a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6zM4.5 9.707V16H8v-4a.5.5 0 01.5-.5h3a.5.5 0 01.5.5v4h3.5V9.707l-5.5-5.5-5.5 5.5z" clip-rule="evenodd"/></svg>
                <div class="menu-mobile-sub">
                    <span>Home</span>
                    <span class="font-size-12">Voltar para página inicial</span>
                </div>
            </a>
        </li>
        <li>
            <a class="menu-mobile-icons" href="https://jcmartim.com.br/servicos">
                <svg class="bi bi-white mr-2" width="32" height="32" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M6 3h8a2 2 0 012 2v10a2 2 0 01-2 2H6a2 2 0 01-2-2V5a2 2 0 012-2zm0 1a1 1 0 00-1 1v10a1 1 0 001 1h8a1 1 0 001-1V5a1 1 0 00-1-1H6z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M10.646 7.646a.5.5 0 01.708 0l2 2a.5.5 0 010 .708l-2 2a.5.5 0 01-.708-.708L12.293 10l-1.647-1.646a.5.5 0 010-.708zm-1.292 0a.5.5 0 00-.708 0l-2 2a.5.5 0 000 .708l2 2a.5.5 0 00.708-.708L7.707 10l1.647-1.646a.5.5 0 000-.708z" clip-rule="evenodd"></path>
                </svg>
                <div class="menu-mobile-sub">
                    <span>Serviços</span>
                    <span class="font-size-12">Websites, E-commerce, Apps, Design e Consultoria</span>
                </div>
            </a>
        </li>
        <li>
            <a class="menu-mobile-icons" href="https://jcmartim.com.br/trabalhos">
                <svg class="bi bi-white mr-2" width="32" height="32" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M4 4.5a.5.5 0 00-.5.5v1a.5.5 0 00.5.5h1a.5.5 0 00.5-.5V5a.5.5 0 00-.5-.5H4zM5 5H4v1h1V5z" clip-rule="evenodd"></path>
                    <path d="M7 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM7.5 9a.5.5 0 000 1h9a.5.5 0 000-1h-9zm0 4a.5.5 0 000 1h9a.5.5 0 000-1h-9z"></path>
                    <path fill-rule="evenodd" d="M3.5 9a.5.5 0 01.5-.5h1a.5.5 0 01.5.5v1a.5.5 0 01-.5.5H4a.5.5 0 01-.5-.5V9zM4 9h1v1H4V9zm0 3.5a.5.5 0 00-.5.5v1a.5.5 0 00.5.5h1a.5.5 0 00.5-.5v-1a.5.5 0 00-.5-.5H4zm1 .5H4v1h1v-1z" clip-rule="evenodd"></path>
                </svg>
                <div class="menu-mobile-sub">
                    <span>Trabalhos</span>
                    <span class="font-size-12">Veja alguns de nossos trabalhos</span>
                </div>
            </a>
        </li>
        <li>
            <a class="menu-mobile-icons" href="https://jcmartim.com.br/processo">
                <svg class="bi bi-white mr-2" width="32" height="32" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16 3H4a1 1 0 00-1 1v12a1 1 0 001 1h12a1 1 0 001-1V4a1 1 0 00-1-1zM4 2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V4a2 2 0 00-2-2H4z" clip-rule="evenodd"></path>
                    <path d="M7.25 8.033h1.32c0-.781.458-1.384 1.36-1.384.685 0 1.313.343 1.313 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.007.463h1.307v-.355c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.326 0-2.786.647-2.754 2.533zm1.562 5.516c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"></path>
                </svg>
                <div class="menu-mobile-sub">
                    <span>Processo</span>
                    <span class="font-size-12">O que? Para quem? O que fará?</span>
                </div>
            </a>
        </li>
        <li>
            <a class="menu-mobile-icons" href="https://jcmartim.com.br/contato">
                <svg class="bi bi-white mr-2" width="32" height="32" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M13 3H7a1 1 0 00-1 1v11a1 1 0 001 1h6a1 1 0 001-1V4a1 1 0 00-1-1zM7 2a2 2 0 00-2 2v11a2 2 0 002 2h6a2 2 0 002-2V4a2 2 0 00-2-2H7z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M10 15a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"></path>
                </svg>
                <div class="menu-mobile-sub">
                    <span>Contato</span>
                    <span class="font-size-12">Vamos conversar!</span>
                </div>
            </a>
        </li>
    </ul>
</aside>`

const header = document.getElementById('header')
header.innerHTML = headerContent

const footerContent = `<div class="container">
<div class="g-3">
    <div class="box">
        <img class="mb-3" src="https://jcmartim.com.br/images/marca-jcmartim.svg" title="Sobre a Jcmartim">
        <p>A Jcmartim é uma empresa de desenvolvimento de softwares personalizados, focada na experiência do usuário, no desenvolvimento responsivo da web e no desenvolvimento de aplicativos móveis e web progressivos. A Jcmartim está cediada no estado do Rio Grande do Sul, capital Porto Alegre.</p>
    </div>
    <div class="box">
        <img class="mb-3" src="https://jcmartim.com.br/images/cotacao-jcmartim.svg" title="Cotação de aplicativos">
        <p>Interessado em ver quanto custa criar um aplicativo?</p>
        <a class="btn btn-default mb-3" href="cotacao.html">Iniciar cotação</a><br>
        <a class="btn btn-default" href="estatistica.html">Ver estatísticas</a>
    </div>
    <div class="box">
        <img class="mb-3" src="https://jcmartim.com.br/images/contato-jcmartim.svg" title="Contato com a Jcmartim">
        <p>Entre em contato conosoco, de forma direta, através de nosso WhatsApp. Estamos sempre prontos a lhe atender!!!</p>
        <p>Horário de atendimento das 09:00 às 12:00 e das 14:00 às 18:00 de segunda a sexta.</p>
        <p><a class="btn btn-default" target="_blank" href="https://api.whatsapp.com/send?phone=555193059643">55 51 9305-9643</a></p>
    </div>
</div>
</div>
<div class="copy">
<p>© <span ano-atual></span> Copyright <span>jcmartim.com.br</span> - Design e Desenvolvimento de Aplicativos Web e Mobile</p>
</div>`

const footer = document.getElementById('footer')
footer.innerHTML = footerContent


const toggle = document.querySelector('#toggle')
const menuMobile = document.querySelector('#m-mobile')

/*Incluir a classe*/
addClassOn = ele => ele.classList.add('on')

/*remover a classe*/
removeClass = ele => ele.classList.remove('on')

/* Verifica se a classe já está inclusa no elem */
toggle.onclick = () => {
    
    if (toggle.classList.contains('on')) {
        /*Se já estiver inclusa remove*/
        removeClass(toggle)
        menuMobile.style.display =  'none'
    } else {
        /*Se não estever inclui!*/
        addClassOn(toggle)
        menuMobile.style.display =  'flex'
    }

}
/*Ano atual*/
const dataAno = document.querySelector('[ano-atual]').innerHTML = new Date().getFullYear()

/*Filter hover*/
function filterOn(ele) {
	ele.style.filter = "grayscale(0%)"
	ele.style.opacity = "1"
}
function filterOff(ele) {
	ele.style.filter = "grayscale(100%)"
	ele.style.opacity = "0.4"
}
